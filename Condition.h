#ifndef CONDITION_H
#define CONDITION_H

#include <vector>

using namespace std;

enum Direction {
	GREATER_EQUAL, LESS_EQUAL, EQUAL, NOT_EQUAL
};

struct Condition {
	int feature_index_;
	double feature_value_;
	Direction direction_;

 public:
  Condition();
	Condition(int feature_index, double feature_value, Direction direction);
	bool CoversInstance(const vector<double>& instance) const;
	void Print() const;
};

#endif

