#include "MlRulesTrainer.h"

#include <algorithm>
#include <limits>
#include <iostream>
#include <fstream>
#include <sstream>
#include <ctime>

using namespace std;

MlRulesTrainer::MlRulesTrainer(bool debug_mode)
		: debug_mode_(debug_mode),
			delta_(0.00001),
			initializer_(&instances_, &ordered_instances_, &labels_,
									 &categorical_features_) {}

void MlRulesTrainer::ReadCSVFile(const string& path) {
	initializer_.ReadCSVFile(path);
}

void MlRulesTrainer::ReadLIBSVMFile(const string& path) {
	initializer_.ReadCSVFile(path);
}	

void MlRulesTrainer::Train(int num_rules, double shrinkage,
													 double subsample_size,	bool allow_not_equal) {
	if (instances_.size() == 0) {
		cout << "Data not loaded." << endl;
		return;
	}

	allow_not_equal_ = allow_not_equal;
	shrinkage_ = shrinkage;
	subsample_ = vector<int>(num_train_samples_ * subsample_size);
	num_features_ = instances_.front().size() - 1;
	vote_values_ = vector<double>(num_train_samples_);
	probabilities_ = vector<double>(num_train_samples_);
	subsample_presence_ = vector<bool>(num_train_samples_);
	initializer_.InitializeTrainingData(num_train_samples_);

	AddDefaultRule();
	for (int i = 1; i < num_rules; i++) {
		InitForRuleBuilding();
		Rule rule = BuildRule();
		rule.weight_ = CalculateWeight(rule);
		if (debug_mode_) {
			cout << "Chosen rule:" << endl;
			initializer_.GetRuleString(rule);
		}
    rule_ensemble_.push_back(rule);
	}
	if (num_rules < rule_ensemble_.size()) {
		cout << "too many rules" << endl;
	}
}

void MlRulesTrainer::TrainAndTest(int num_rules, double shrinkage,
		double subsample_size, bool allow_not_equal, int num_folds,
		double* average_time, double* average_accuracy) {
	if (instances_.size() == 0) {
		cout << "Data not loaded." << endl;
		*average_time = 0;
		*average_accuracy = 0;
		return;
	}
	const int num_test_samples = instances_.size() / num_folds;
	num_train_samples_ = instances_.size() - num_test_samples;
	double sum_accuracy = 0;
	double time = 0;
	for (int i = 0; i < num_folds; i++) {
		SwapFolds(i, num_folds);

		clock_t start = clock();
		Train(num_rules, shrinkage, subsample_size, allow_not_equal);
		clock_t end = clock();
		time += end - start;

		int correct_predictions = 0;
		for (int i = num_train_samples_; i < instances_.size(); i++) {
			int prediction = Predict(instances_[i]);
			if (prediction == labels_[i]) {
				correct_predictions++;
			}
		}
		SwapFolds(i, num_folds);
		Clear();
		const double num_test_samples = instances_.size() - num_train_samples_;
		const double accuracy = (double)(correct_predictions) / num_test_samples;
		sum_accuracy += accuracy;
	}
	*average_time = time / CLOCKS_PER_SEC / num_folds;
	*average_accuracy = sum_accuracy / num_folds;
}

void MlRulesTrainer::SwapFolds(int fold, int num_folds) {
	if (fold + 1 == num_folds) {
		return;
	}
	const int num_test_samples = instances_.size() / num_folds;
	const int index_feature = instances_.front().size() - 1;
	for (int i = 0; i < num_test_samples; i++) {
		const int test_sample = fold * num_test_samples + i;
		const int swap_sample = instances_.size() - num_test_samples + i;
		const vector<double> swap = instances_[swap_sample];
		instances_[swap_sample] = instances_[test_sample];
		instances_[swap_sample][index_feature] = swap_sample;
		instances_[test_sample] = swap;
		instances_[test_sample][index_feature] = test_sample;
		const int swap_label = labels_[swap_sample];
		labels_[swap_sample] = labels_[test_sample];
		labels_[test_sample] = swap_label;
	}
}

void MlRulesTrainer::Clear() {
	rule_ensemble_.clear();
	ordered_instances_.clear();
	vote_values_.clear();
	probabilities_.clear();
	subsample_.clear();
	subsample_presence_.clear();
}

void MlRulesTrainer::AddDefaultRule() {
	double positive = 0;
	double negative = 0;
	for (int i = 0; i < num_train_samples_; i++) {
		if (labels_[i]) positive++;
		else negative++;
	}
	double weight = (negative > 0) ? log(positive/negative) / 2 : 1;
	Rule rule;
	rule.weight_ = weight;
	rule_ensemble_.push_back(rule);
	if (debug_mode_) {
		cout << "Default rule:" << endl;
		cout << initializer_.GetRuleString(rule);
	}
}

string MlRulesTrainer::Predict(const vector<Feature>& features) {
	vector<double> values = initializer_.GetValueVector(features);
	return initializer_.GetLabelName(Predict(values));
}

string MlRulesTrainer::PredictCSVSample(const string& sample) {
	vector<double> values = initializer_.GetValueVectorFromCSV(sample);
	return initializer_.GetLabelName(Predict(values));
}

string MlRulesTrainer::PredictLIBSVMSample(const string& sample) {
	vector<double> values = initializer_.GetValueVectorFromLIBSVM(sample);
	return initializer_.GetLabelName(Predict(values));
}


void MlRulesTrainer::PrintRules() {
	cout << "MlRulesTrainer:" << endl;
	for (const Rule& rule : rule_ensemble_) {
		cout << initializer_.GetRuleString(rule);
	}
}

Rule MlRulesTrainer::BuildRule() {
	bool max_achieved = false;
	double best_loss = 0;
	Rule rule;
	rule.weight_ = 1;
	Condition best_condition;
	vector<bool> covered_instances(subsample_presence_);
	while (!max_achieved) {
		double current_loss;
		Condition condition =
				ChooseBestCut(rule, covered_instances, &current_loss);
		rule.conditions_.push_back(condition);
		if (debug_mode_) {
			cout << "best condition atm has loss " << current_loss << endl;
			cout << initializer_.GetCutString(condition);
		}
		if (current_loss - best_loss > delta_) {
			best_loss = current_loss;
			UpdateCoveredInstances(condition, &covered_instances);
		} else {
			rule.conditions_.pop_back();
			max_achieved = true;
		}
	}
	if (debug_mode_) {
		cout << "loss = " << best_loss << " for rule: " << endl;
		cout << initializer_.GetRuleString(rule);
	}
	return rule;
}

Condition MlRulesTrainer::ChooseBestCut(const Rule& rule,
		const vector<bool>& covered_instances, double* loss) {
	double best_loss = 0;
	Condition best_cut;
	vector<Direction> directions;
	directions.push_back(GREATER_EQUAL);
	directions.push_back(LESS_EQUAL);
	for (int feature_index = 0; feature_index < num_features_; feature_index++) {
		const vector<int> instance_order = ordered_instances_[feature_index];
		double current_loss;
		Condition current_cut = (categorical_features_[feature_index]) ?
				ChooseBestCategoricalCut(rule, covered_instances,
						feature_index, instance_order, &current_loss) :
				ChooseBestDirectedCut(rule, covered_instances,
						feature_index, instance_order, &current_loss);
		if (current_loss - best_loss > delta_) {
			best_cut = current_cut;
			best_loss = current_loss;
		}
	}
	if (debug_mode_) {
		cout << "best cut: ";
		cout << initializer_.GetCutString(best_cut);
	}
	*loss = best_loss;
	return best_cut;
}

Condition MlRulesTrainer::ChooseBestCategoricalCut(
		const Rule& rule, const vector<bool>& covered_instances, int feature_index,
		const vector<int>& instance_order, double* loss) {
	double full_loss = 0;
	if (allow_not_equal_) {
		for (int instance_index : instance_order) {
			const vector<double>& instance = instances_[instance_index];
			if (!covered_instances[instance_index]) {
				continue;
			}
			full_loss -= probabilities_[instance_index];
			if (labels_[instance_index]) {
				full_loss += 1;
			}
		}
	}
	double best_loss = 0;
	double last_value = instances_[instance_order.front()][feature_index];
	double current_value = last_value;
	Condition best_cut = Condition(feature_index, last_value, EQUAL);
	double current_loss = 0;
	double not_equal_loss = 0;
	for (int instance_index : instance_order) {
		const vector<double>& instance = instances_[instance_index];
		if (!covered_instances[instance_index]) {
			continue;
		}
		current_value = instance[feature_index];
		if (current_value > last_value) {
			if (abs(current_loss) - best_loss > delta_) {
				best_loss = abs(current_loss);
				best_cut.feature_value_ = last_value;
				best_cut.direction_ = EQUAL;
			}
			if (debug_mode_) {
				cout << "loss = " << current_loss << " for cut ";
				cout << initializer_.GetCutString(Condition(feature_index, last_value,
																										EQUAL));
				Rule rule_copy(rule);
				rule_copy.conditions_.push_back(Condition(feature_index, last_value,
																									EQUAL));
				if (abs(GetSubsampleLoss(rule_copy) - current_loss) > delta_) {
					cout << "ACTUAL loss = " << GetSubsampleLoss(rule_copy) << endl;
				}
			}
			if (allow_not_equal_) {
				not_equal_loss = full_loss - current_loss;
				if (abs(not_equal_loss) - best_loss > delta_) {
					best_loss = abs(not_equal_loss);
					best_cut.feature_value_ = last_value;
					best_cut.direction_ = NOT_EQUAL;
				}
				if (debug_mode_) {
					cout << "loss = " << not_equal_loss << " for cut ";
					cout << initializer_.GetCutString(Condition(feature_index,
																											last_value, NOT_EQUAL));
					Rule rule_copy(rule);
					rule_copy.conditions_.push_back(Condition(feature_index, last_value,
																										NOT_EQUAL));
					if (abs(GetSubsampleLoss(rule_copy) - not_equal_loss) > delta_) {
						cout << "ACTUAL loss = " << GetSubsampleLoss(rule_copy) << endl;
					}
					rule_copy.conditions_.pop_back();
				}
			}
			current_loss = 0;
			last_value = current_value;
		}
		current_loss -= probabilities_[instance_index];
		if (labels_[instance_index]) {
			current_loss += 1;
		}
	}
	if (abs(current_loss) - best_loss > delta_) {
		best_loss = abs(current_loss);
		best_cut.feature_value_ = last_value;
		best_cut.direction_ = EQUAL;
	}
	if (allow_not_equal_) {
		not_equal_loss = full_loss - current_loss;
		if (abs(not_equal_loss) - best_loss > delta_) {
			best_loss = abs(not_equal_loss);
			best_cut.feature_value_ = last_value;
			best_cut.direction_ = NOT_EQUAL;
		}
	}
	if (debug_mode_) {
		cout << "loss = " << current_loss << " for cut ";
		cout << initializer_.GetCutString(Condition(feature_index, last_value,
																								EQUAL));
		Rule rule_copy(rule);
		rule_copy.conditions_.push_back(Condition(feature_index, last_value,
																							EQUAL));
		if (abs(GetSubsampleLoss(rule_copy) - current_loss) > delta_) {
			cout << "ACTUAL loss = " << GetSubsampleLoss(rule_copy) << endl;
		}
		rule_copy.conditions_.pop_back();
		if (allow_not_equal_) {
			cout << "loss = " << not_equal_loss << " for cut ";
			cout << initializer_.GetCutString(Condition(feature_index, last_value,
																									NOT_EQUAL));
			rule_copy.conditions_.push_back(Condition(feature_index, last_value,
																								NOT_EQUAL));
			if (abs(GetSubsampleLoss(rule_copy) - not_equal_loss) > delta_) {
				cout << "ACTUAL loss = " << GetSubsampleLoss(rule_copy) << endl;
			}
			rule_copy.conditions_.pop_back();
		}
	}
	*loss = best_loss;
	return best_cut;
}

Condition MlRulesTrainer::ChooseBestDirectedCut(
		const Rule& rule, const vector<bool>& covered_instances, int feature_index,
		const vector<int>& instance_order, double* loss) {
	if (instance_order.empty()) {
		*loss = 0;
		return Condition(feature_index, 0, LESS_EQUAL);
	}
	double full_loss = 0;
	for (const int& instance_index : instance_order) {
		const vector<double>& instance = instances_[instance_index];
		if (!covered_instances[instance_index]) {
			continue;
		}
		full_loss -= probabilities_[instance_index];
		if (labels_[instance_index]) {
			full_loss += 1;
		}
	}
	double best_loss = 0;
	double current_loss = 0;
	double last_value = instances_[instance_order.front()][feature_index];
	Condition best_cut = Condition(feature_index, 0, LESS_EQUAL);
	for (const int& instance_index : instance_order) {
		const vector<double>& instance = instances_[instance_index];
		if (!covered_instances[instance_index]) {
			continue;
		}
		const double current_value = instance[feature_index];
		if (current_value > last_value) {
			const double new_cut = (current_value + last_value) / 2;
			if (abs(current_loss) - best_loss > delta_) {
				best_loss = abs(current_loss);
				best_cut.feature_value_ = new_cut;
				best_cut.direction_ = LESS_EQUAL;
			}
			if (debug_mode_) {
				cout << "loss = " << current_loss << " for cut ";
				cout << initializer_.GetCutString(Condition(feature_index, new_cut,
																										LESS_EQUAL));
				Rule rule_copy(rule);
				rule_copy.conditions_.push_back(Condition(feature_index,
																									new_cut, LESS_EQUAL));
				if (abs(GetSubsampleLoss(rule_copy) - current_loss) > delta_) {
					cout << "ACTUAL loss = " << GetSubsampleLoss(rule_copy) << endl;
				}
			}
			const double greater_equal_loss = full_loss - current_loss;
			if (abs(greater_equal_loss) - best_loss > delta_) {
				best_loss = abs(greater_equal_loss);
				best_cut.feature_value_ = new_cut;
				best_cut.direction_ = GREATER_EQUAL;
			}
			if (debug_mode_) {
				cout << "loss = " << greater_equal_loss << " for cut ";
				cout << initializer_.GetCutString(Condition(feature_index, new_cut,
																										GREATER_EQUAL));
				Rule rule_copy(rule);
				rule_copy.conditions_.push_back(Condition(feature_index,
																									new_cut, GREATER_EQUAL));
				if (abs(GetSubsampleLoss(rule_copy) - greater_equal_loss) > delta_) {
					cout << "ACTUAL loss = " << GetSubsampleLoss(rule_copy) << endl;
				}
			}
			last_value = current_value;
		}
		current_loss -= probabilities_[instance_index];
		if (labels_[instance_index]) {
			current_loss += 1;
		}
	}
	*loss = best_loss;
	return best_cut;
}

void MlRulesTrainer::UpdateCoveredInstances(const Condition& condition,
																		 vector<bool>* covered_instances) {
	for (int i = 0; i < covered_instances->size(); i++) {
		if ((*covered_instances)[i]) {
			(*covered_instances)[i] = condition.CoversInstance(instances_[i]);
		}
	}
}

double MlRulesTrainer::GetLoss(const Rule& rule) {
	double loss = 0;
	for (int i = 0; i < num_train_samples_; i++) {
		const int correct_label = labels_[i];
		if (rule.CoversInstance(instances_[i])) {
			loss += probabilities_[i];
			if (correct_label) {
				loss -= 1;
			}
		}
	}
	return -loss;
}

double MlRulesTrainer::GetSubsampleLoss(const Rule& rule) {
	double loss = 0;
	for (const int i : subsample_) {
		const int correct_label = labels_[i];
		if (rule.CoversInstance(instances_[i])) {
			loss += probabilities_[i];
			if (correct_label) {
				loss -= 1;
			}
		}
	}
	return -loss;
}

double MlRulesTrainer::GetProbability(int instance_index) {
	return 1 / (1 + exp(-2 * vote_values_[instance_index]));
}

double MlRulesTrainer::GetVoteValue(const vector<double>& instance) {
	double vote = 0;
	for (const Rule& rule : rule_ensemble_) {
		if (rule.CoversInstance(instance)) {
			vote += rule.weight_;
		}
	}
	return vote;
}

double MlRulesTrainer::CalculateWeight(const Rule& rule) {
	return GetLoss(rule) / GetSecondLoss(rule) * shrinkage_;
}

double MlRulesTrainer::GetSecondLoss(const Rule& rule) {
	double loss = 0;
	for (int i = 0; i < num_train_samples_; i++) {
		const vector<double>& instance = instances_[i];
		const int correct_label = labels_[i];
		if (rule.CoversInstance(instance)) {
			const double p = probabilities_[i];
			loss += p * (1 - p);
		}
	}
	return loss;
}

bool MlRulesTrainer::Predict(const vector<double>& values) {
	double vote = GetVoteValue(values);
	return vote > 0;
}


void MlRulesTrainer::InitForRuleBuilding() {
	for (int i = 0; i < num_train_samples_; i++) {
		vote_values_[i] = GetVoteValue(instances_[i]);
	}
	for (int i = 0; i < num_train_samples_; i++) {
		probabilities_[i] = GetProbability(i);
	}
	vector<int> indices(num_train_samples_);
	for (int i = 0; i < num_train_samples_; i++) {
		indices[i] = i;
	}
	for (int i = 0; i < subsample_presence_.size(); i++) {
		subsample_presence_[i] = false;
	}
	for (int i = 0; i < subsample_.size(); i++) {
		int swap_index =
				((double)(rand()) / RAND_MAX) * (indices.size() - 1 - i) + i;
		subsample_[i] = indices[swap_index];
		indices[swap_index] = indices[i];
		subsample_presence_[subsample_[i]] = true;
	}
}