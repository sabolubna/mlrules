#include "MlRulesTrainer.h"

#include <iostream>
#include <fstream>
#include <cstdio>
#include <sstream>
#include <string>
#include <ctime>

using namespace std;

bool TestOnDataset(const string& dataset_path, double expected_accuracy) {
	cout << "Testing on " << dataset_path << endl;
	cout << "Expected accuracy: " << expected_accuracy << endl;
	const int folds = 10;
	MlRulesTrainer rules = MlRulesTrainer(false);
	rules.ReadCSVFile(dataset_path);
	double average_accuracy = 0;
	double average_time = 0;
	rules.TrainAndTest(500, 0.1, 0.5, true, folds, &average_time,
										 &average_accuracy);
	cout << "Single training took on average " << average_time << " sec." << endl;
	cout << "Average accuracy: " << average_accuracy * 100 << "%" << endl;
	if (average_accuracy < expected_accuracy) {
		cout << "FAILED" << endl << endl;
		return true;
	}
	cout << "PASSED" << endl << endl;
	return false;
}

int main(int argc, char** argv) {
  ofstream out("output.txt");
  streambuf *coutbuf = cout.rdbuf();
  cout.rdbuf(out.rdbuf());

	int result = 0;
	result += TestOnDataset("haberman.txt", 0.7333);
	result += TestOnDataset("credit-g.txt", 0.7585);
	result += TestOnDataset("credit-a.txt", 0.8451);
	result += TestOnDataset("ionosphere.txt", 0.9283);
	result += TestOnDataset("colic.txt", 0.8294);
	result += TestOnDataset("hepatitis.txt", 0.8264);
	result += TestOnDataset("sonar.txt", 0.8309);
	result += TestOnDataset("heart-statlog.txt", 0.7967);
	result += TestOnDataset("liver-disorders.txt", 0.7110);
	result += TestOnDataset("vote.txt", 0.9560);
	result += TestOnDataset("heart-c.txt", 0.8105);
	result += TestOnDataset("heart-h.txt", 0.801);
	result += TestOnDataset("breast-w.txt", 0.9655);
	result += TestOnDataset("sick.txt", 0.9796);
	result += TestOnDataset("tic-tac-toe.txt", 0.9496);
	result += TestOnDataset("spambase.txt", 0.9506);
	result += TestOnDataset("cylinder-bands.txt", 0.8043);
	result += TestOnDataset("kr-vs-kp.txt", 0.9937);
	if (result == 0) {
		cout << "All tests pass" << endl;
	} else {
		cout << "Not all tests pass" << endl;
	}

  cout.rdbuf(coutbuf);
	cout << "Done." << endl;

	string str;
	getline(cin, str);
}