#include "DataInitializer.h"

#include "utils.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <limits>
#include <set>
#include <sstream>

DataInitializer::DataInitializer(vector<vector<double>>* instances,
		vector<vector<int>>* ordered_instances, vector<int>* labels,
		vector<bool>* categorical_features)
	: instances_(instances), ordered_instances_(ordered_instances),
		labels_(labels), categorical_features_(categorical_features) {}

void DataInitializer::ReadCSVFile(const string& path) {
	vector<Instance> instances;
	string line_string;
  ifstream file(path);
  if (!file.is_open()) {
		cout << "Unable to open file" << endl;
		return;
	}
  while (getline(file, line_string)) {
		vector<string> tokens;
		char* line = strtok((char*)line_string.c_str(), ",");
		while (line != NULL) {
			tokens.push_back(line);
			line = strtok(NULL, ",");
		}
		string label = tokens.back();
		tokens.pop_back();
		vector<Feature> features;
		for (int i = 0; i < tokens.size(); i++) {
			stringstream stream;
			stream << i;
			if (tokens[i] == "?") {
				features.push_back(Feature(stream.str(),
																	 numeric_limits<double>::quiet_NaN()));
			} else if (strspn(tokens[i].c_str(), "-.0123456789") == 
								 tokens[i].size()) {
				features.push_back(Feature(stream.str(), stod(tokens[i])));
			} else {
				features.push_back(Feature(stream.str(), tokens[i]));
			}
		}
		instances.push_back(Instance(features, label));
  }
  file.close();
	InitializeData(instances);
}

void DataInitializer::ReadLIBSVMFile(const string& path) {
	vector<Instance> instances;
	string line_string;
  ifstream file(path);
  if (!file.is_open()) {
		cout << "Unable to open file" << endl;
		return;
	}
  while (getline(file, line_string)) {
		vector<string> tokens;
		char* line = strtok((char*)line_string.c_str(), " ");
		while (line != NULL) {
			tokens.push_back(line);
			line = strtok(NULL, " ");
		}
		string label = tokens.front();
		vector<Feature> features;
		for (int i = 1; i < tokens.size(); i++) {
			char* line = strtok((char*)tokens[i].c_str(), ":");
			string feature = line;
			string value = strtok(NULL, ":");
			features.push_back(Feature(feature, stod(value)));
		}
		instances.push_back(Instance(features, label));
  }
  file.close();
	InitializeData(instances);
}

void DataInitializer::InitializeData(const vector<Instance>& instances) {
	set<int> categorical;
	for (int i = 0; i < instances.size(); i++) {
		if (categorical.size() > 15) {
			cout << "";
		}
		Instance instance = instances[i];
		labels_->push_back(GetLabelIndex(instance.label_));
		instances_->push_back(vector<double>(feature_map_.size()));
		for (Feature feature : instance.features_) {
			int index = GetFeatureIndex(feature.name_);
			if (feature.category_ != "") {
				categorical.insert(index);
				int category = GetCategoryIndex(feature.category_);
				(*instances_)[i][index] = category;
			} else {
				(*instances_)[i][index] = feature.value_;
			}
		}
	}
	*categorical_features_ = vector<bool>(feature_map_.size());
	for (int index : categorical) {
		(*categorical_features_)[index] = true;
	}
	ShuffleData();
	for (int i = 0; i < instances_->size(); i++) {
		(*instances_)[i].push_back(i);
	}
	num_train_samples_ = instances_->size();
}

void DataInitializer::InitializeTrainingData(int num_train_samples) {
	num_train_samples_ = num_train_samples;
	ordered_instances_->clear();
	for (int i = 0; i < feature_map_.size(); i++) {
		ordered_instances_->push_back(vector<int>());
		sort(instances_->begin(), instances_->begin() + num_train_samples_,
				 InstanceCmp(i));
		for (int j = 0; j < num_train_samples_; j++) {
			if ((*instances_)[j][i] == (*instances_)[j][i]) {
				(*ordered_instances_)[i].push_back((*instances_)[j].back());
			}
		}
	}
	sort(instances_->begin(), instances_->end(),
			 InstanceCmp(feature_map_.size()));
}

vector<double> DataInitializer::GetValueVector(
		const vector<Feature>& features) {
	vector<double> values = vector<double>(features_.size());
	for (const Feature& feature : features) {
		int index = 0;
		if (feature_map_.find(feature.name_) != feature_map_.end()) {
			index = feature_map_[feature.name_];
		} else {
			continue;
		}
		if ((*categorical_features_)[index]) {
			values[index] = category_map_[feature.category_];
		} else {
			values[index] = feature.value_;
		}
	}
	return values;
}

vector<double> DataInitializer::GetValueVectorFromCSV(const string& sample) {
	vector<string> tokens;
	char* line = strtok((char*)sample.c_str(), ",");
	while (line != NULL) {
		tokens.push_back(line);
		line = strtok(NULL, ",");
	}
	vector<Feature> features;
	for (int i = 0; i < tokens.size(); i++) {
		stringstream stream;
		stream << i;
		if (tokens[i] == "?") {
			features.push_back(Feature(stream.str(),
																	numeric_limits<double>::quiet_NaN()));
		} else if (strspn(tokens[i].c_str(), "-.0123456789") == 
								tokens[i].size()) {
			features.push_back(Feature(stream.str(), stod(tokens[i])));
		} else {
			features.push_back(Feature(stream.str(), tokens[i]));
		}
	}
	return GetValueVector(features);
}

vector<double> DataInitializer::GetValueVectorFromLIBSVM(
		const string& sample) {
	vector<string> tokens;
	char* line = strtok((char*)sample.c_str(), " ");
	while (line != NULL) {
		tokens.push_back(line);
		line = strtok(NULL, " ");
	}
	vector<Feature> features;
	for (int i = 0; i < tokens.size(); i++) {
		char* line = strtok((char*)tokens[i].c_str(), ":");
		string feature = line;
		string value = strtok(NULL, ":");
		features.push_back(Feature(feature, stod(value)));
	}
	return GetValueVector(features);
}

string DataInitializer::GetCutString(const Condition& condition) {
	stringstream cut_string;
	cut_string << features_[condition.feature_index_];
	switch (condition.direction_) {
		case GREATER_EQUAL:
			cut_string << " >= " << condition.feature_value_ << endl;
			break;
		case LESS_EQUAL:
			cut_string << " <= " << condition.feature_value_ << endl;
			break;
		case EQUAL:
			cut_string << " = " << categories_[condition.feature_value_] << endl;
			break;
		case NOT_EQUAL:
			cut_string << " != " << categories_[condition.feature_value_] << endl;
			break;
	}
	return cut_string.str();
}

string DataInitializer::GetRuleString(const Rule& rule) {
	stringstream rule_string;
	rule_string << "if" << endl;
	for (const Condition& condition: rule.conditions_) {
		rule_string << GetCutString(condition);
	}
	rule_string << "then weight " << rule.weight_ << endl;
	return rule_string.str();
}

string DataInitializer::GetLabelName(bool label_index) {
	return label_names_[label_index];
}

int DataInitializer::GetFeatureIndex(const string& feature_name) {
	if (feature_map_.find(feature_name) != feature_map_.end()) {
		return feature_map_[feature_name];
	}

	for (vector<double>& instance : *instances_) {
		instance.push_back(0);
	}
	int new_index = feature_map_.size();
	feature_map_[feature_name] = new_index;
	features_.push_back(feature_name);
	return new_index;
}

// TODO: refactor two methods below into one
int DataInitializer::GetLabelIndex(const string& label_name) {
	if (label_map_.find(label_name) != label_map_.end()) {
		return label_map_[label_name];
	}
	int new_index = label_map_.size();
	label_map_[label_name] = new_index;
	label_names_.push_back(label_name);
	return new_index;
}

int DataInitializer::GetCategoryIndex(const string& category_name) {
	if (category_map_.find(category_name) != category_map_.end()) {
		return category_map_[category_name];
	}
	int new_index = category_map_.size();
	category_map_[category_name] = new_index;
	categories_.push_back(category_name);
	return new_index;
}

void DataInitializer::ShuffleData() {
	for (int i = 0; i < instances_->size() - 2; i++) {
		int swap_index =
				((double)(rand()) / RAND_MAX) * (instances_->size() - 1 - i) + i;
		vector<double> swap = (*instances_)[i];
		(*instances_)[i] = (*instances_)[swap_index];
		(*instances_)[swap_index] = swap;
		double swap_label = (*labels_)[i];
		(*labels_)[i] = (*labels_)[swap_index];
		(*labels_)[swap_index] = swap_label;
	}
}