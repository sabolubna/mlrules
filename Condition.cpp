#include "Condition.h"

#include <math.h>
#include <iostream>

using namespace std;

 Condition::Condition() : feature_index_(0), feature_value_(0), 
		            direction_(GREATER_EQUAL) {}

Condition::Condition(int feature_index, double feature_value, Direction direction)
		: feature_index_(feature_index), feature_value_(feature_value), 
		  direction_(direction) {}

bool Condition::CoversInstance(const vector<double>& instance) const {
	if (instance[feature_index_] != instance[feature_index_]) return false;
	switch (direction_) {
		case GREATER_EQUAL:
			return instance[feature_index_] >= feature_value_;
		case LESS_EQUAL:
			return instance[feature_index_] <= feature_value_;
		case EQUAL:
			return instance[feature_index_] == feature_value_;
		case NOT_EQUAL:		
			return instance[feature_index_] != feature_value_;
	}
}

void Condition::Print() const {
	cout << feature_index_;
	switch (direction_) {
		case GREATER_EQUAL:
			cout << " >= ";
			break;
		case LESS_EQUAL:
			cout << " <= ";
			break;
		case EQUAL:
			cout << " = ";
			break;
		case NOT_EQUAL:
			cout << " != ";
			break;
	}
	cout << feature_value_ << endl;
}