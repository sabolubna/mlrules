#pragma once

#include "Condition.h"
#include "DataInitializer.h"
#include "Rule.h"
#include "utils.h"

#include <map>
#include <set>
#include <string>
#include <vector>

using namespace std;

// Main class performing the MlRules training.
class MlRulesTrainer {
 public:
	// Constructor with debug_mode flag. In the debug mode, not only are the loss
	// values displayed, but they are also compared with values calculated by
	// simple, not optimized method, which makes the training significantly
	// longer (complexity O(m*n^2) instead of O(m*n), where m - number of
	// features, n - number of instances.
	MlRulesTrainer(bool debug_mode);
	// Reads input from CSV file and initializes data.
	void ReadCSVFile(const string& path);
	// Reads input from LIBSVM file and initializes data
	void ReadLIBSVMFile(const string& path);
	// Runs the training using given parameters:
	// num_rules - number of rules that will be generated;
	// shrinkage - parameter for shrinking the base classifier being added to the
	// ensemble;
	// subsample_size - ratio between subsample used for generating single rule
	// and full dataset;
	// allow_not_equal - flag indicating whether conditions of the form "x != a"
	// for nominal attributes should be considered.
	void Train(int num_rules, double shrinkage, double subsample_size,
		bool allow_not_equal);
	// Runs the trainings using x-fold cross validation and returns average
	// time spent on training and accuracy achieved on the test sets.
	void TrainAndTest(int num_rules, double shrinkage, double subsample_size,
		bool allow_not_equal, int num_folds, double* average_time,
		double* average_accuracy);
	// Returns prediction for single instance.
	string Predict(const vector<Feature>& features);
	// Reads the input for a single sample, same as used for training and returns
	// predicted class.
	string PredictCSVSample(const string& sample);
	// Reads the input for a single sample, same as used for training and returns
	// predicted class.
	string PredictLIBSVMSample(const string& sample);
	// Prints the full rule ensemble.
	void PrintRules();

 private:
	void AddDefaultRule();
	Rule BuildRule();
	Condition ChooseBestCut(const Rule& rule,
		const vector<bool>& covered_instances, double* loss);
	Condition ChooseBestCategoricalCut(const Rule& rule,
		const vector<bool>& covered_instances, int feature_index,
		const vector<int>& instance_order, double* loss);
	Condition ChooseBestDirectedCut(const Rule& rule,
		const vector<bool>& covered_instances, int feature_index,
		const vector<int>& instance_order, double* loss);
	void UpdateCoveredInstances(const Condition& condition,
		vector<bool>* covered_instances);
	double GetLoss(const Rule& rule);
	double GetSubsampleLoss(const Rule& rule);
	double GetProbability(int instance_index);
	double GetVoteValue(const vector<double>& instance);
	double CalculateWeight(const Rule& rule);
	double GetSecondLoss(const Rule& rule);
	bool Predict(const vector<double>& values);
	void InitForRuleBuilding();
	void SwapFolds(int fold, int num_folds);
	void Clear();

 private:
 	vector<Rule> rule_ensemble_;
 	vector<vector<double>> instances_;
	// Vector of indices of instances ordered by value of each of the features.
 	vector<vector<int>> ordered_instances_;
	vector<int> labels_;
	// Flags indicating whether a feature is categorical.
	vector<bool> categorical_features_;
	DataInitializer initializer_;
	int num_features_;
	int num_train_samples_;
	double shrinkage_;
	vector<double> vote_values_;
	vector<double> probabilities_;
	// Indices of instances chosen randomly as a subsample.
	vector<int> subsample_;
	// Flags indicating whether each instance is currently in the subsample.
	vector<bool> subsample_presence_;
	// Flag determining whether to allow for conditions of the form
	// feature != value.
	bool allow_not_equal_;
	// Flag determining whether to print debug messages.
	bool debug_mode_;
	// Set to 1e-5. Used for comparison of doubles.
	const double delta_;
};

