// File for simple structs and util functions.

#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <vector>

using namespace std;

struct Feature {
	string name_;
	double value_;
	string category_;

 public:
	Feature(const string& name, double value) 
		: name_(name), value_(value), category_("") {}
	Feature(const string& name, const string& category) 
		: name_(name), value_(0), category_(category) {}
};

struct Instance {
	string name_;
	vector<Feature> features_;
	string label_;

 public:
	Instance(const vector<Feature>& features, string label) 
		: name_(""), features_(features), label_(label) {}
};

struct InstanceCmp {
 public:
	InstanceCmp(int feature_index) {
		feature_index_ = feature_index;
	}
  inline bool operator() (const vector<double>& instance,
													const vector<double>& other) {
		if (instance[feature_index_] != instance[feature_index_]) return false;
		if (other[feature_index_] != other[feature_index_]) return true;
      return (instance[feature_index_] < other[feature_index_]);
  }
 private:
	int feature_index_;
};

#endif