#ifndef RULE_H
#define RULE_H

#include "Condition.h"

#include <vector>

using namespace std;

struct Rule {
	vector<Condition> conditions_;
	double weight_;

 public:
	bool CoversInstance(const vector<double>& instance) const;
	void Print();
};

#endif