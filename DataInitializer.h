#pragma once

#include "Condition.h"
#include "Rule.h"
#include "utils.h"

#include <map>
#include <string>
#include <vector>

using namespace std;

// Class initializing the data for MlRules training, i.e. reading the input 
// and transforming it into vectors of doubles. Keeps all the mappings from
// objects' names to indices.
class DataInitializer {
 public:
	// Constructor is given pointers to MLRulesTrainer members that will
	// be filled with data.
	DataInitializer(vector<vector<double>>* instances_,
		vector<vector<int>>* ordered_instances_, vector<int>* labels_,
		vector<bool>* categorical_features);
	// Reads the input from CSV file and initializes all the structures for
	// the training.
	void ReadCSVFile(const string& path);
	// Reads the input from LIBSVM file and initializes all the structures for
	// the training.
	void ReadLIBSVMFile(const string& path);
	// Fills instances_ with data.
 	void InitializeData(const vector<Instance>& instances);
	// Initializes data that depends on train-test samples split.
	void InitializeTrainingData(int num_train_samples);
	// Translates Feature structs into vector of doubles with values under
	// the indices corresponding to the feature.
	vector<double> GetValueVector(const vector<Feature>& features);
	// Translates string CSV sample into vector of doubles.
	vector<double> GetValueVectorFromCSV(const string& sample);
	// Translates string LIBSVM sample into vector of doubles.
	vector<double> GetValueVectorFromLIBSVM(const string& sample);
	// Returns debug string, replacing indices with names.
	string GetCutString(const Condition& condition);
	// Returns debug string with all conditions in the rule.
	string GetRuleString(const Rule& rule);
	// Returns name of the label.
	string GetLabelName(bool label_index);

 private:
 	int GetFeatureIndex(const string& feature_name);
 	int GetLabelIndex(const string& label_name);
 	int GetCategoryIndex(const string& label_name);
	void ShuffleData();
	
 	vector<vector<double>>* instances_;
 	// Vector of indices of instances ordered by value of each of the features.
 	vector<vector<int>>* ordered_instances_;
	vector<int>* labels_;
	// Flags indicating whether a feature is categorical.
	vector<bool>* categorical_features_;
	int num_train_samples_;
 	map<string, int> feature_map_;
	// Feature names sorted by index (index to name map).
	vector<string> features_;
	map<string, int> category_map_;
	// Categories sorted by index (index to category map).
	vector<string> categories_;
	map<string, int> label_map_;
	// Label names sorted by index (index to name map).
	vector<string> label_names_;
};

