#include "Rule.h"

#include <iostream>

using namespace std;

bool Rule::CoversInstance(const vector<double>& instance) const {
	for (const Condition& condition : conditions_) {
		if (!condition.CoversInstance(instance)) {
			return false;
		}
	}
	return true;
}

void Rule::Print() {
	cout << "if" << endl;
	for (const Condition& condition : conditions_) {
		condition.Print();
	}
	cout << "then weight " << weight_ << endl;
}